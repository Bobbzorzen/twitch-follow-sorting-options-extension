$(document).ready(function() {
    console.log("Initialized twitch follow sorting")

    function convertViewerCountToInteger(viewerCountAsString) {
        let viewerCountAsInteger = viewerCountAsString;
        if(viewerCountAsString.match(/^[0-9]*$/)) {
            viewerCountAsInteger = parseInt(viewerCountAsString)
        } else if (viewerCountAsString.match(/^[0-9]*.*K$/)) {
            viewerCountAsInteger = parseFloat(viewerCountAsString.substring(0, viewerCountAsString.length-1))*1000
        } else if (viewerCountAsString.match(/^Offline$/)) {
            viewerCountAsInteger = -1;
        }
        return viewerCountAsInteger;
    }

    function getCurrentViewerCount(channel) {
        if (channel) {
            return convertViewerCountToInteger($(channel).find("span").text());
        }
        return -1
    }

    function sortChannels(channelA, channelB) {
        const channelAViewerCount = getCurrentViewerCount(channelA);
        const channelBViewerCount = getCurrentViewerCount(channelB);
        if (channelAViewerCount > channelBViewerCount) {
            return -1;
        }
        if (channelAViewerCount < channelBViewerCount) {
            return 1;
        }
        return 0;
    }

    function sortFollowList() {
        let followsList = $("div#sideNav div[aria-label='Followed Channels'] > div.tw-relative.tw-transition-group")
        let allChannels = followsList.children()
        let allOnlineChannels = allChannels.filter((channel) => {
            return getCurrentViewerCount($(allChannels[channel])) !== -1
        })
        let allOfflineChannels = allChannels.filter((channel) => {
            return getCurrentViewerCount($(allChannels[channel])) === -1
        })

        allOnlineChannels.sort(sortChannels)

        followsList.empty()
        followsList.append(allOnlineChannels, allOfflineChannels);
    }

    setTimeout(sortFollowList, 2000);
    setInterval(sortFollowList, 10000);
});