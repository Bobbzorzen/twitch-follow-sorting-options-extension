document.addEventListener('DOMContentLoaded', function() {
    console.log("Running code in popup");

    var notify = function () {
        var opt = {
            type: "basic",
            title: "Primary Title",
            message: "Primary message to display",
            iconUrl: "icon.png"
        }

        chrome.notifications.create(opt);
    };
}, false);